import request from '@/utils/request'

// 查询客户基本信息列表
export function listEnterpriseInfo(query) {
  return request({
    url: '/business/enterpriseInfo/list',
    method: 'get',
    params: query
  })
}

// 查询客户基本信息详细
export function getEnterpriseInfo(id) {
  return request({
    url: '/business/enterpriseInfo/' + id,
    method: 'get'
  })
}
export function getEnterpriseInfoDetails(id) {
  return request({
    url: '/business/enterpriseInfo/details/' + id,
    method: 'get'
  })
}

// 新增客户基本信息
export function addEnterpriseInfo(data) {
  return request({
    url: '/business/enterpriseInfo',
    method: 'post',
    data: data
  })
}

// 修改客户基本信息
export function updateEnterpriseInfo(data) {
  return request({
    url: '/business/enterpriseInfo',
    method: 'put',
    data: data
  })
}

// 删除客户基本信息
export function delEnterpriseInfo(id) {
  return request({
    url: '/business/enterpriseInfo/' + id,
    method: 'delete'
  })
}

//查询所有客户信息
export function getEnterprise() {
  return request({
    url: '/business/enterpriseInfo/allList',
    method: 'get',
  })
}
