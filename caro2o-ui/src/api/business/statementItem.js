import request from '@/utils/request'

// 查询结算单明细列表
export function listStatementItem(query) {
  return request({
    url: '/business/statementItem/list',
    method: 'get',
    params: query
  })
}

// 新增结算单明细
export function addStatementItem(data) {
  return request({
    url: '/business/statementItem',
    method: 'post',
    data: data
  })
}

// 支付结算单明细
export function payStatement(id) {
  return request({
    url: '/business/statementItem/pay/'+id,
    method: 'post',
  })
}
