import request from '@/utils/request'

// 查询库存管理列表
export function listInventory(query) {
  return request({
    url: '/business/inventory/list',
    method: 'get',
    params: query
  })
}

// 查询库存管理详细
export function getInventory(id) {
  return request({
    url: '/business/inventory/' + id,
    method: 'get'
  })
}

// 新增库存管理
export function addInventory(data) {
  return request({
    url: '/business/inventory',
    method: 'post',
    headers : {
      'Content-Type' : 'multipart/form-data'
    },
    data: data
  })
}

// 修改库存管理
export function updateInventory(data) {
  return request({
    url: '/business/inventory',
    method: 'put',
    data: data
  })
}

// 删除库存管理
export function delInventory(id) {
  return request({
    url: '/business/inventory/' + id,
    method: 'delete'
  })
}

//查询出入库明细
export function OPIPOfwarehouse(id) {
  return request({
    url: '/business/inventory/warehouse/' + id,
    method: 'get'
  })
}
