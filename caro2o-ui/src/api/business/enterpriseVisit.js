import request from '@/utils/request'

// 查询客户拜访列表
export function listEnterpriseVisit(query) {
  return request({
    url: '/business/enterpriseVisit/list',
    method: 'get',
    params: query
  })
}
export function getContectsList() {
  return request({
    url: '/business/enterpriseVisit/getContectsList',
    method: 'get',
  })
}
export function getEnterpriseInfoList() {
  return request({
    url: '/business/enterpriseVisit/getEnterpriseInfoList',
    method: 'get',
  })
}

// 查询客户拜访详细
export function getEnterpriseVisit(id) {
  return request({
    url: '/business/enterpriseVisit/' + id,
    method: 'get'
  })
}

// 新增客户拜访
export function addEnterpriseVisit(data) {
  return request({
    url: '/business/enterpriseVisit',
    method: 'post',
    data: data
  })
}

// 修改客户拜访
export function updateEnterpriseVisit(data) {
  return request({
    url: '/business/enterpriseVisit',
    method: 'put',
    data: data
  })
}

// 删除客户拜访
export function delEnterpriseVisit(id) {
  return request({
    url: '/business/enterpriseVisit/' + id,
    method: 'delete'
  })
}
