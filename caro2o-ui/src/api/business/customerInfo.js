import request from '@/utils/request'

// 查询客户档案列表
export function listCustomerInfo(query) {
  return request({
    url: '/business/customerInfo/list',
    method: 'get',
    params: query
  })
}

// 查询客户档案详细
export function getCustomerInfo(id) {
  return request({
    url: '/business/customerInfo/' + id,
    method: 'get'
  })
}

// 新增客户档案
export function addCustomerInfo(data) {
  return request({
    url: '/business/customerInfo',
    method: 'post',
    data: data
  })
}

// 修改客户档案
export function updateCustomerInfo(data) {
  return request({
    url: '/business/customerInfo',
    method: 'put',
    data: data
  })
}

// 删除客户档案
export function delCustomerInfo(id) {
  return request({
    url: '/business/customerInfo/' + id,
    method: 'delete'
  })
}
