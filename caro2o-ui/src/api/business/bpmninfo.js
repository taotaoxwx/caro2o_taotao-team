import request from '@/utils/request'

// 查询流程定义明细列表
export function listBpmninfo(query) {
  return request({
    url: '/business/bpmninfo/list',
    method: 'get',
    params: query
  })
}

// 查询流程定义明细详细
export function getBpmninfo(id) {
  return request({
    url: '/business/bpmninfo/' + id,
    method: 'get'
  })
}

// 新增流程定义明细
export function addBpmninfo(data) {
  return request({
    url: '/business/bpmninfo',
    method: 'post',
    data: data
  })
}

// 修改流程定义明细
export function updateBpmninfo(data) {
  return request({
    url: '/business/bpmninfo',
    method: 'put',
    data: data
  })
}

// 删除流程定义明细
export function cancelBpmninfo(id) {
  return request({
    url: '/business/bpmninfo/cancel/' + id,
    method: 'post'
  })
}

// 部署流程定义
export function deployBpmnInfo(data) {
  return request({
    url: '/business/bpmninfo/deploy',
    method: 'post',
    headers : {
      'Content-Type' : 'multipart/form-data'
    },
    data : data,
  })
}

// 查看流程定义明细
export function getProcessResource(id , type) {
  return request({
    url: `/business/bpmninfo/getProcess/${id}/${type}` ,
    method: 'get'
  })
}




