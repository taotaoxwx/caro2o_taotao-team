package cn.wolfcode.business.statement.service.impl;

import java.util.Date;
import java.util.List;

import cn.wolfcode.business.appointment.domain.BusAppointment;
import cn.wolfcode.business.appointment.util.RegexUtils;
import cn.wolfcode.business.appointment.util.VehiclePlateNoUtil;
import cn.wolfcode.business.customerInfo.domain.BusCustomerInfo;
import cn.wolfcode.business.customerInfo.mapper.BusCustomerInfoMapper;
import cn.wolfcode.business.statement.domain.vo.BusStatementQuery;
import cn.wolfcode.business.statement.domain.vo.BusStatementVO;
import cn.wolfcode.business.statementItem.mapper.BusStatementItemMapper;
import cn.wolfcode.common.utils.DateUtils;
import cn.wolfcode.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.statement.mapper.BusStatementMapper;
import cn.wolfcode.business.statement.domain.BusStatement;
import cn.wolfcode.business.statement.service.IBusStatementService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 结算单Service业务层处理
 * 
 * @author wolfcode
 * @date 2023-05-31
 */
@Service
@Transactional
public class BusStatementServiceImpl implements IBusStatementService 
{
    @Autowired
    private BusStatementMapper busStatementMapper;
    @Autowired
    private BusCustomerInfoMapper busCustomerInfoMapper;
    /**
     * 查询结算单
     * 
     * @param id 结算单主键
     * @return 结算单
     */
    @Override
    public BusStatement selectBusStatementById(Long id)
    {
        return busStatementMapper.selectBusStatementById(id);
    }

    /**
     * 查询结算单列表
     * 
     * @param busStatement 结算单
     * @return 结算单
     */
    @Override
    public List<BusStatement> selectBusStatementList(BusStatement busStatement)
    {
        return busStatementMapper.selectBusStatementList(busStatement);
    }

    /**
     * 新增结算单
     * 
     * @param busStatementVO 结算单
     * @return 结果
     */
    @Override
    public int insertBusStatement(BusStatementVO busStatementVO)
    {
        if(busStatementVO == null){
            throw new RuntimeException("非法操作");
        }
        if(!RegexUtils.isPhoneLegal(busStatementVO.getCustomerPhone())){
            throw new RuntimeException("手机号码不合法");
        }
        //4 车牌号码校验
        if(VehiclePlateNoUtil.getVehiclePlateNo(busStatementVO.getLicensePlate()) == null){
            throw new RuntimeException("车牌号码不合法");
        }
        BusStatement busStatement = new BusStatement();
        BeanUtils.copyProperties(busStatementVO,busStatement);

        busStatement.setActualArrivalTime(new Date());
        busStatement.setCreateTime(new Date());
        busStatement.setStatus(BusStatement.STATUS_CONSUME);
        return busStatementMapper.insertBusStatement(busStatement);
    }

    /**
     * 修改结算单
     * 
     * @param busStatementVO 结算单
     * @return 结果
     */
    @Override
    public int updateBusStatement(BusStatementVO busStatementVO)
    {
        if(busStatementVO == null){
            throw new RuntimeException("非法操作");
        }
        if(!RegexUtils.isPhoneLegal(busStatementVO.getCustomerPhone())){
            throw new RuntimeException("手机号码不合法");
        }
        //4 车牌号码校验
        if(VehiclePlateNoUtil.getVehiclePlateNo(busStatementVO.getLicensePlate()) == null){
            throw new RuntimeException("车牌号码不合法");
        }
        BusStatement busStatement = busStatementMapper.selectBusStatementById(busStatementVO.getId());
        if(busStatement == null){
            throw new RuntimeException("非法操作");
        }
        if(!BusStatement.STATUS_CONSUME.equals(busStatement.getStatus())){
            throw new RuntimeException("用户状态必须消费中");
        }
        BeanUtils.copyProperties(busStatementVO,busStatement);
        return busStatementMapper.updateBusStatement(busStatement);
    }

    /**
     * 批量删除结算单
     *
     * @param ids 需要删除的结算单主键
     * @return 结果
     */
    @Override
    public int deleteBusStatementByIds(Long[] ids)
    {
        return busStatementMapper.deleteBusStatementByIds(ids);
    }

    /**
     * 删除结算单信息
     * 
     * @param id 结算单主键
     * @return 结果
     */
    @Override
    public int deleteBusStatementById(Long id)
    {
        if(id == null){
            throw new RuntimeException("非法操作");
        }
        BusStatement busStatement = busStatementMapper.selectBusStatementById(id);
        if(busStatement == null){
            throw new RuntimeException("非法操作");
        }
        if(!BusStatement.STATUS_CONSUME.equals(busStatement.getStatus())){
            throw new RuntimeException("用户状态必须为消费中");
        }
        busStatementMapper.deleteRelation(id);
        return busStatementMapper.updateStatementIsDeleteById(id);
    }

    @Override
    public List<BusStatement> selectBusStatementQOList(BusStatementQuery busStatementQO) {
        return busStatementMapper.selectBusStatementQOList(busStatementQO);
    }

    @Override
    public int synMsg(Long id) {
        BusStatement busStatement = busStatementMapper.selectBusStatementById(id);

        BusCustomerInfo busCustomerInfo = new BusCustomerInfo();
        busCustomerInfo.setCustomerName(busStatement.getCustomerName());
        busCustomerInfo.setCustomerPhone(busStatement.getCustomerPhone());
        busCustomerInfo.setGender(0);
        busCustomerInfo.setClerk(SecurityUtils.getUsername());
        busCustomerInfo.setEnterTime(new Date());
        if(busCustomerInfoMapper.selectBusCustomerInfoByPhone(busCustomerInfo.getCustomerPhone()) == null){
            busCustomerInfoMapper.insertBusCustomerInfo(busCustomerInfo);
        }
        return 1;
    }
}
