package cn.wolfcode.business.statement.domain.vo;

import cn.wolfcode.business.serviceItem.domain.BusServiceItem;
import cn.wolfcode.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusStatementQuery {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    public Date getEndTime(){
        Calendar calendar = Calendar.getInstance();
        if(endTime != null){
            calendar.setTime(endTime);
            calendar.add(Calendar.DAY_OF_MONTH,1);
            return calendar.getTime();
        }
        return null;
    }
}
