package cn.wolfcode.business.category.domain.vo;

import cn.wolfcode.business.category.domain.BusCategory;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 分类管理对象 bus_category
 * 
 * @author Mbester
 * @date 2023-06-16
 */
@Getter
@Setter
@ToString
public class BusCategoryParentVO extends BaseEntity
{

    /** 分类id */
    private Long id;

    /** 上级分类的id */
    private Long parentId;

    /** 分类名称 */
    private String name;

    /** 上级分类名称 */
    private String parentName;


}
