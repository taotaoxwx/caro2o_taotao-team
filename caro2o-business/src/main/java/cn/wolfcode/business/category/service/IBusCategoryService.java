package cn.wolfcode.business.category.service;

import java.util.List;
import cn.wolfcode.business.category.domain.BusCategory;
import cn.wolfcode.business.category.domain.vo.BusCategoryParentVO;
import cn.wolfcode.business.category.domain.vo.BusOptionsVO;

/**
 * 分类管理Service接口
 * 
 * @author Mbester
 * @date 2023-06-16
 */
public interface IBusCategoryService 
{
    /**
     * 查询分类管理
     * 
     * @param id 分类管理主键
     * @return 分类管理
     */
    public BusCategoryParentVO selectBusCategoryById(Long id);

    /**
     * 查询分类管理列表
     * 
     * @param busCategory 分类管理
     * @return 分类管理集合
     */
    public List<BusCategory> selectBusCategoryList(BusCategory busCategory);

    /**
     * 新增分类管理
     * 
     * @param busCategory 分类管理
     * @return 结果
     */
    public Long insertBusCategory(BusCategory busCategory);

    /**
     * 修改分类管理
     * 
     * @param busCategory 分类管理
     * @return 结果
     */
    public int updateBusCategory(BusCategory busCategory);

    /**
     * 批量删除分类管理
     * 
     * @param ids 需要删除的分类管理主键集合
     * @return 结果
     */
    public int deleteBusCategoryByIds(Long[] ids);

    /**
     * 删除分类管理信息
     * 
     * @param id 分类管理主键
     * @return 结果
     */
    public int deleteBusCategoryById(Long id);

    /**
     * 获取上级分类名称
     * @return
     */
    List<BusCategoryParentVO> getParentNames();

    /**
     * 获取上级分类搜索选项
     * @return
     */
    List<BusCategory> getOptions();
}
