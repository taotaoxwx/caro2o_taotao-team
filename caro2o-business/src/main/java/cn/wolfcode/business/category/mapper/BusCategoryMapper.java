package cn.wolfcode.business.category.mapper;

import java.util.List;
import cn.wolfcode.business.category.domain.BusCategory;
import cn.wolfcode.business.category.domain.vo.BusCategoryParentVO;
import cn.wolfcode.business.category.domain.vo.BusOptionsVO;
import org.apache.ibatis.annotations.Param;

/**
 * 分类管理Mapper接口
 * 
 * @author Mbester
 * @date 2023-06-16
 */
public interface BusCategoryMapper 
{
    /**
     * 查询分类管理
     * 
     * @param id 分类管理主键
     * @return 分类管理
     */
    public BusCategory selectBusCategoryById(Long id);

    /**
     * 查询分类管理列表
     * 
     * @param busCategory 分类管理
     * @return 分类管理集合
     */
    public List<BusCategory> selectBusCategoryList(BusCategory busCategory);

    /**
     * 新增分类管理
     * 
     * @param busCategory 分类管理
     * @return 结果
     */
    public int insertBusCategory(BusCategory busCategory);

    /**
     * 修改分类管理
     * 
     * @param busCategory 分类管理
     * @return 结果
     */
    public int updateBusCategory(BusCategory busCategory);

    /**
     * 删除分类管理
     * 
     * @param id 分类管理主键
     * @return 结果
     */
    public int deleteBusCategoryById(Long id);

    /**
     * 批量删除分类管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusCategoryByIds(Long[] ids);

    /**
     * 获取上级分类名称和id
     * @return
     */
    List<BusCategoryParentVO> getParentNames();

    /**
     * 搜索上级路径
     * @param
     * @return
     */
    String selectPath(Long id);

    BusCategory selectBusCategoryByName(String name);

    List<BusCategory> selectAllList();

    List<BusCategory> selectPathById(String parentId);

    List<BusCategory> selectBusCategoryByIds(@Param("ids") List<String> ids);

    List<String> getAllPath();

}
