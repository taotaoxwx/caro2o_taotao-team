package cn.wolfcode.business.inventory.mapper;

import cn.wolfcode.business.inventory.domain.InventoryManagement;
import cn.wolfcode.business.inventory.domain.vo.ParticularsVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 库存管理Mapper接口
 *
 * @author sakura
 * @date 2023-06-16
 */
public interface InventoryManagementMapper
{
    /**
     * 查询库存管理
     *
     * @param id 库存管理主键
     * @return 库存管理
     */
    public InventoryManagement selectInventoryManagementById(Long id);

    /**
     * 查询库存管理列表
     *
     * @param inventoryManagement 库存管理
     * @return 库存管理集合
     */
    public List<InventoryManagement> selectInventoryManagementList(InventoryManagement inventoryManagement);

    /**
     * 新增库存管理
     *
     * @param inventoryManagement 库存管理
     * @return 结果
     */
    public int insertInventoryManagement(InventoryManagement inventoryManagement);

    /**
     * 修改库存管理
     *
     * @param inventoryManagement 库存管理
     * @return 结果
     */
    public int updateInventoryManagement(InventoryManagement inventoryManagement);

    /**
     * 删除库存管理
     *
     * @param id 库存管理主键
     * @return 结果
     */
    public int deleteInventoryManagementById(Long id);

    /**
     * 批量删除库存管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteInventoryManagementByIds(Long[] ids);

    //仓库中新增物品
    void insertByWarehouse(@Param("warehouseId") Integer warehouseId, @Param("id") Long id);
    //仓库中修改物品
    void updateByWarehouse(@Param("warehouseId") Integer warehouseId, @Param("id") Long id);
    //仓库中删除物品
    void deleteByWarehouse(Long[] ids);

    List<ParticularsVO> queryByRepertoryId(Long id);
}
