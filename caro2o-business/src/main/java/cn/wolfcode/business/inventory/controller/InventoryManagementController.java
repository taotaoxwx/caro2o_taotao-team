package cn.wolfcode.business.inventory.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.inventory.domain.InventoryManagement;
import cn.wolfcode.business.inventory.domain.vo.ParticularsVO;
import cn.wolfcode.business.inventory.service.IInventoryManagementService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 库存管理Controller
 *
 * @author sakura
 * @date 2023-06-16
 */
@RestController
@RequestMapping("/business/inventory")
@Transactional
public class InventoryManagementController extends BaseController
{
    @Autowired
    private IInventoryManagementService inventoryManagementService;

    /**
     * 查询库存管理列表
     */
    @PreAuthorize("@ss.hasPermi('business:inventory:list')")
    @GetMapping("/list")
    public TableDataInfo list(InventoryManagement inventoryManagement)
    {
        startPage();
        List<InventoryManagement> list = inventoryManagementService.selectInventoryManagementList(inventoryManagement);
        return getDataTable(list);
    }

    /**
     * 导出库存管理列表
     */
    @PreAuthorize("@ss.hasPermi('business:inventory:export')")
    @Log(title = "库存管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, InventoryManagement inventoryManagement)
    {
        List<InventoryManagement> list = inventoryManagementService.selectInventoryManagementList(inventoryManagement);
        ExcelUtil<InventoryManagement> util = new ExcelUtil<InventoryManagement>(InventoryManagement.class);
        util.exportExcel(response, list, "库存管理数据");
    }

    /**
     * 获取库存管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:inventory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(inventoryManagementService.selectInventoryManagementById(id));
    }

    /**
     * 新增库存管理
     */
    @PreAuthorize("@ss.hasPermi('business:inventory:add')")
    @Log(title = "库存管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(InventoryManagement inventoryManagement,MultipartFile file) throws IOException {
        return toAjax(inventoryManagementService.insertInventoryManagement(inventoryManagement,file));
    }

    /**
     * 修改库存管理
     */
    @PreAuthorize("@ss.hasPermi('business:inventory:edit')")
    @Log(title = "库存管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(InventoryManagement inventoryManagement,MultipartFile file) throws IOException {

        return toAjax(inventoryManagementService.updateInventoryManagement(inventoryManagement,file));
    }

    /**
     * 删除库存管理
     */
    @PreAuthorize("@ss.hasPermi('business:inventory:remove')")
    @Log(title = "库存管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(inventoryManagementService.deleteInventoryManagementByIds(ids));
    }

    /**
     * 查看库存物品明细表
     */
    @PreAuthorize("@ss.hasPermi('business:inventory:warehouse')")
    @Log(title = "库存管理", businessType = BusinessType.DELETE)
    @GetMapping("/warehouse/{id}")
    public AjaxResult warehouse(@PathVariable Long id)
    {
        List<ParticularsVO> list =  inventoryManagementService.getParticulars(id);
        return AjaxResult.success(list);
    }
}
