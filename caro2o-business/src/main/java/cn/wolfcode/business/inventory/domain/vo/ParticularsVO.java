package cn.wolfcode.business.inventory.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class ParticularsVO {
    //明细id
    private Integer id;
    //出入库类型 1入库 0出库
    private Integer type;
    //操作时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operationTime;
    //物品名称
    private String goods;
    //品牌
    private String brand;
    //仓库
    //private Integer warehouse;
    //仓库名
    private String warehouseName;
    //数量
    private Integer quantity;
    //单价
    private BigDecimal unitPrice;
    //库存物品id
    private Integer goodsid;
}
