package cn.wolfcode.business.inventory.service;

import cn.wolfcode.business.inventory.domain.InventoryManagement;
import cn.wolfcode.business.inventory.domain.vo.ParticularsVO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 库存管理Service接口
 *
 * @author sakura
 * @date 2023-06-16
 */
public interface IInventoryManagementService
{
    /**
     * 查询库存管理
     *
     * @param id 库存管理主键
     * @return 库存管理
     */
    public InventoryManagement selectInventoryManagementById(Long id);

    /**
     * 查询库存管理列表
     *
     * @param inventoryManagement 库存管理
     * @return 库存管理集合
     */
    public List<InventoryManagement> selectInventoryManagementList(InventoryManagement inventoryManagement);

    /**
     * 新增库存管理
     *
     * @param inventoryManagement 库存管理
     * @return 结果
     */
    public int insertInventoryManagement(InventoryManagement inventoryManagement, MultipartFile file) throws IOException;

    /**
     * 修改库存管理
     *
     * @param inventoryManagement 库存管理
     * @return 结果
     */
    public int updateInventoryManagement(InventoryManagement inventoryManagement, MultipartFile file) throws IOException;

    /**
     * 批量删除库存管理
     *
     * @param ids 需要删除的库存管理主键集合
     * @return 结果
     */
    public int deleteInventoryManagementByIds(Long[] ids);

    /**
     * 删除库存管理信息
     *
     * @param id 库存管理主键
     * @return 结果
     */
    public int deleteInventoryManagementById(Long id);

    /**
     * 出入库明细表
     * @param id  库存物品id
     * @return
     */
    List<ParticularsVO> getParticulars(Long id);
}
