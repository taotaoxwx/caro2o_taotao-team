package cn.wolfcode.business.enterpriseVisit.service.impl;

import java.util.Date;
import java.util.List;

import cn.wolfcode.business.contacts.domain.EnterpriseContacts;
import cn.wolfcode.business.contacts.mapper.EnterpriseContactsMapper;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;
import cn.wolfcode.business.enterpriseInfo.mapper.EnterpriseInfoMapper;
import cn.wolfcode.business.enterpriseVisit.domain.vo.EnterpriseVisitVO;
import cn.wolfcode.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.enterpriseVisit.mapper.EnterpriseVisitMapper;
import cn.wolfcode.business.enterpriseVisit.domain.EnterpriseVisit;
import cn.wolfcode.business.enterpriseVisit.service.IEnterpriseVisitService;
import org.springframework.util.Assert;

/**
 * 客户拜访Service业务层处理
 * 
 * @author taotao
 * @date 2023-06-17
 */
@Service
public class EnterpriseVisitServiceImpl implements IEnterpriseVisitService 
{
    @Autowired
    private EnterpriseVisitMapper enterpriseVisitMapper;
    @Autowired
    private EnterpriseContactsMapper enterpriseContactsMapper;
    @Autowired
    private EnterpriseInfoMapper enterpriseInfoMapper;

    /**
     * 查询客户拜访
     * 
     * @param id 客户拜访主键
     * @return 客户拜访
     */
    @Override
    public EnterpriseVisit selectEnterpriseVisitById(Long id)
    {
        return enterpriseVisitMapper.selectEnterpriseVisitById(id);
    }

    /**
     * 查询客户拜访列表
     * 
     * @param enterpriseVisit 客户拜访
     * @return 客户拜访
     */
    @Override
    public List<EnterpriseVisit> selectEnterpriseVisitList(EnterpriseVisit enterpriseVisit)
    {
        return enterpriseVisitMapper.selectEnterpriseVisitList(enterpriseVisit);
    }

    /**
     * 新增客户拜访
     * 
     * @param enterpriseVisitVO 客户拜访
     * @return 结果
     */
    @Override
    public int insertEnterpriseVisit(EnterpriseVisitVO enterpriseVisitVO)
    {
        Assert.notNull(enterpriseVisitVO,"非法操作");
        Assert.notNull(enterpriseVisitVO.getEnterpriseName(),"客户名称不能为空");
        Assert.notNull(enterpriseVisitVO.getContacts(),"联系人不能为空");
        Assert.notNull(enterpriseVisitVO.getVisitReason(),"拜访原因不能为空");
        Assert.notNull(enterpriseVisitVO.getVisitType(),"拜访方式不能为空");
        Assert.notNull(enterpriseVisitVO.getVisitTime(),"拜访时间不能为空");
        Assert.state(enterpriseVisitVO.getVisitTime().before(new Date()),"时间不可超过现在");
        Assert.notNull(enterpriseVisitVO.getCommunicationSituation(),"交流情况不能为空");
        EnterpriseVisit enterpriseVisit = new EnterpriseVisit();
        BeanUtils.copyProperties(enterpriseVisitVO,enterpriseVisit);
        enterpriseVisit.setClerk(SecurityUtils.getUsername());
        enterpriseVisit.setEntryTime(new Date());
        return enterpriseVisitMapper.insertEnterpriseVisit(enterpriseVisit);
    }

    /**
     * 修改客户拜访
     * 
     * @param enterpriseVisitVO 客户拜访
     * @return 结果
     */
    @Override
    public int updateEnterpriseVisit(EnterpriseVisitVO enterpriseVisitVO)
    {
        Assert.notNull(enterpriseVisitVO,"非法操作");
        Assert.notNull(enterpriseVisitVO.getEnterpriseName(),"客户名称不能为空");
        Assert.notNull(enterpriseVisitVO.getContacts(),"联系人不能为空");
        Assert.notNull(enterpriseVisitVO.getVisitReason(),"拜访原因不能为空");
        Assert.notNull(enterpriseVisitVO.getVisitType(),"拜访方式不能为空");
        Assert.notNull(enterpriseVisitVO.getVisitTime(),"拜访时间不能为空");
        Assert.state(enterpriseVisitVO.getVisitTime().before(new Date()),"时间不可超过现在");
        Assert.notNull(enterpriseVisitVO.getCommunicationSituation(),"交流情况不能为空");
        EnterpriseVisit enterpriseVisit = enterpriseVisitMapper.selectEnterpriseVisitById(enterpriseVisitVO.getId());
        Assert.notNull(enterpriseVisit,"非法操作");
        BeanUtils.copyProperties(enterpriseVisitVO,enterpriseVisit);
        return enterpriseVisitMapper.updateEnterpriseVisit(enterpriseVisit);
    }

    /**
     * 批量删除客户拜访
     * 
     * @param ids 需要删除的客户拜访主键
     * @return 结果
     */
    @Override
    public int deleteEnterpriseVisitByIds(Long[] ids)
    {
        return enterpriseVisitMapper.deleteEnterpriseVisitByIds(ids);
    }

    /**
     * 删除客户拜访信息
     * 
     * @param id 客户拜访主键
     * @return 结果
     */
    @Override
    public int deleteEnterpriseVisitById(Long id)
    {
        return enterpriseVisitMapper.deleteEnterpriseVisitById(id);
    }

    @Override
    public List<EnterpriseContacts> getContectsList() {

        return enterpriseContactsMapper.getContectsList();
    }

    @Override
    public List<EnterpriseInfo> getEnterpriseInfoList() {

        return enterpriseInfoMapper.getEnterpriseNameList();
    }
}
