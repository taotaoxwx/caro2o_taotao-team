package cn.wolfcode.business.enterpriseVisit.service;

import java.util.List;

import cn.wolfcode.business.contacts.domain.EnterpriseContacts;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;
import cn.wolfcode.business.enterpriseVisit.domain.EnterpriseVisit;
import cn.wolfcode.business.enterpriseVisit.domain.vo.EnterpriseVisitVO;

/**
 * 客户拜访Service接口
 * 
 * @author taotao
 * @date 2023-06-17
 */
public interface IEnterpriseVisitService 
{
    /**
     * 查询客户拜访
     * 
     * @param id 客户拜访主键
     * @return 客户拜访
     */
    public EnterpriseVisit selectEnterpriseVisitById(Long id);

    /**
     * 查询客户拜访列表
     * 
     * @param enterpriseVisit 客户拜访
     * @return 客户拜访集合
     */
    public List<EnterpriseVisit> selectEnterpriseVisitList(EnterpriseVisit enterpriseVisit);

    /**
     * 新增客户拜访
     * 
     * @param enterpriseVisit 客户拜访
     * @return 结果
     */
    public int insertEnterpriseVisit(EnterpriseVisitVO enterpriseVisit);

    /**
     * 修改客户拜访
     * 
     * @param enterpriseVisit 客户拜访
     * @return 结果
     */
    public int updateEnterpriseVisit(EnterpriseVisitVO enterpriseVisit);

    /**
     * 批量删除客户拜访
     * 
     * @param ids 需要删除的客户拜访主键集合
     * @return 结果
     */
    public int deleteEnterpriseVisitByIds(Long[] ids);

    /**
     * 删除客户拜访信息
     * 
     * @param id 客户拜访主键
     * @return 结果
     */
    public int deleteEnterpriseVisitById(Long id);

    public List<EnterpriseContacts> getContectsList();

    public List<EnterpriseInfo> getEnterpriseInfoList();

}
