package cn.wolfcode.business.enterpriseVisit.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 客户拜访对象 enterprise_visit
 * 
 * @author taotao
 * @date 2023-06-17
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EnterpriseVisitVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String enterpriseName;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contacts;

    /** 拜访原因 */
    @Excel(name = "拜访原因")
    private String visitReason;

    /** 拜访方式 */
    @Excel(name = "拜访方式")
    private Integer visitType;

    /** 拜访时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "拜访时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date visitTime;

    /** 交流情况 */
    @Excel(name = "交流情况")
    private String communicationSituation;

}
