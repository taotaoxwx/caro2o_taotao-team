package cn.wolfcode.business.enterpriseVisit.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

/**
 * 客户拜访对象 enterprise_visit
 * 
 * @author taotao
 * @date 2023-06-17
 */
public class EnterpriseVisit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String enterpriseName;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contacts;

    /** 拜访原因 */
    @Excel(name = "拜访原因")
    private String visitReason;

    /** 拜访方式 */
    @Excel(name = "拜访方式")
    private Integer visitType;

    /** 拜访时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "拜访时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date visitTime;

    /** 交流情况 */
    @Excel(name = "交流情况")
    private String communicationSituation;

    /** 录入人 */
    @Excel(name = "录入人")
    private String clerk;

    /** 录入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date entryTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEnterpriseName(String enterpriseName) 
    {
        this.enterpriseName = enterpriseName;
    }

    public String getEnterpriseName() 
    {
        return enterpriseName;
    }
    public void setContacts(String contacts) 
    {
        this.contacts = contacts;
    }

    public String getContacts() 
    {
        return contacts;
    }
    public void setVisitReason(String visitReason) 
    {
        this.visitReason = visitReason;
    }

    public String getVisitReason() 
    {
        return visitReason;
    }
    public void setVisitType(Integer visitType) 
    {
        this.visitType = visitType;
    }

    public Integer getVisitType() 
    {
        return visitType;
    }
    public void setVisitTime(Date visitTime) 
    {
        this.visitTime = visitTime;
    }

    public Date getVisitTime() 
    {
        return visitTime;
    }
    public void setCommunicationSituation(String communicationSituation) 
    {
        this.communicationSituation = communicationSituation;
    }

    public String getCommunicationSituation() 
    {
        return communicationSituation;
    }
    public void setClerk(String clerk) 
    {
        this.clerk = clerk;
    }

    public String getClerk() 
    {
        return clerk;
    }
    public void setEntryTime(Date entryTime) 
    {
        this.entryTime = entryTime;
    }

    public Date getEntryTime() 
    {
        return entryTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("enterpriseName", getEnterpriseName())
            .append("contacts", getContacts())
            .append("visitReason", getVisitReason())
            .append("visitType", getVisitType())
            .append("visitTime", getVisitTime())
            .append("communicationSituation", getCommunicationSituation())
            .append("clerk", getClerk())
            .append("entryTime", getEntryTime())
            .toString();
    }
}
