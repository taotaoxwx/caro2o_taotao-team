package cn.wolfcode.business.enterpriseVisit.mapper;

import java.util.List;
import cn.wolfcode.business.enterpriseVisit.domain.EnterpriseVisit;

/**
 * 客户拜访Mapper接口
 * 
 * @author taotao
 * @date 2023-06-17
 */
public interface EnterpriseVisitMapper 
{
    /**
     * 查询客户拜访
     * 
     * @param id 客户拜访主键
     * @return 客户拜访
     */
    public EnterpriseVisit selectEnterpriseVisitById(Long id);

    /**
     * 查询客户拜访列表
     * 
     * @param enterpriseVisit 客户拜访
     * @return 客户拜访集合
     */
    public List<EnterpriseVisit> selectEnterpriseVisitList(EnterpriseVisit enterpriseVisit);

    /**
     * 新增客户拜访
     * 
     * @param enterpriseVisit 客户拜访
     * @return 结果
     */
    public int insertEnterpriseVisit(EnterpriseVisit enterpriseVisit);

    /**
     * 修改客户拜访
     * 
     * @param enterpriseVisit 客户拜访
     * @return 结果
     */
    public int updateEnterpriseVisit(EnterpriseVisit enterpriseVisit);

    /**
     * 删除客户拜访
     * 
     * @param id 客户拜访主键
     * @return 结果
     */
    public int deleteEnterpriseVisitById(Long id);

    /**
     * 批量删除客户拜访
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEnterpriseVisitByIds(Long[] ids);
}
