package cn.wolfcode.business.customerInfo.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusCustomerInfoVO {
    private Long id;
    private String customerName;
    private String customerPhone;
    private Integer gender;
}
