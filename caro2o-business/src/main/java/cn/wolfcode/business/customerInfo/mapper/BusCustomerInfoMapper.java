package cn.wolfcode.business.customerInfo.mapper;

import java.util.List;
import cn.wolfcode.business.customerInfo.domain.BusCustomerInfo;
import cn.wolfcode.business.customerInfo.domain.vo.BusCustomerInfoQO;

/**
 * 客户档案Mapper接口
 * 
 * @author wolfcode
 * @date 2023-06-10
 */
public interface BusCustomerInfoMapper 
{
    /**
     * 查询客户档案
     * 
     * @param id 客户档案主键
     * @return 客户档案
     */
    public BusCustomerInfo selectBusCustomerInfoById(Long id);

    /**
     * 查询客户档案列表
     * 
     * @param busCustomerInfo 客户档案
     * @return 客户档案集合
     */
    public List<BusCustomerInfo> selectBusCustomerInfoList(BusCustomerInfoQO busCustomerInfo);

    /**
     * 新增客户档案
     * 
     * @param busCustomerInfo 客户档案
     * @return 结果
     */
    public int insertBusCustomerInfo(BusCustomerInfo busCustomerInfo);

    /**
     * 修改客户档案
     * 
     * @param busCustomerInfo 客户档案
     * @return 结果
     */
    public int updateBusCustomerInfo(BusCustomerInfo busCustomerInfo);

    /**
     * 删除客户档案
     * 
     * @param id 客户档案主键
     * @return 结果
     */
    public int deleteBusCustomerInfoById(Long id);

    /**
     * 批量删除客户档案
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusCustomerInfoByIds(Long[] ids);

    public BusCustomerInfo selectBusCustomerInfoByPhone(String customerPhone);
}
