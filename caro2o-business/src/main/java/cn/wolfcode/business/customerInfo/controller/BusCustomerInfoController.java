package cn.wolfcode.business.customerInfo.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.customerInfo.domain.vo.BusCustomerInfoQO;
import cn.wolfcode.business.customerInfo.domain.vo.BusCustomerInfoVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.customerInfo.domain.BusCustomerInfo;
import cn.wolfcode.business.customerInfo.service.IBusCustomerInfoService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 客户档案Controller
 * 
 * @author wolfcode
 * @date 2023-06-10
 */
@RestController
@RequestMapping("/business/customerInfo")
public class BusCustomerInfoController extends BaseController
{
    @Autowired
    private IBusCustomerInfoService busCustomerInfoService;

    /**
     * 查询客户档案列表
     */
    @PreAuthorize("@ss.hasPermi('business:customerInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusCustomerInfoQO busCustomerInfo)
    {
        startPage();
        List<BusCustomerInfo> list = busCustomerInfoService.selectBusCustomerInfoList(busCustomerInfo);
        return getDataTable(list);
    }

    /**
     * 导出客户档案列表
     */
    @PreAuthorize("@ss.hasPermi('business:customerInfo:export')")
    @Log(title = "客户档案", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusCustomerInfoQO busCustomerInfo)
    {
        List<BusCustomerInfo> list = busCustomerInfoService.selectBusCustomerInfoList(busCustomerInfo);
        ExcelUtil<BusCustomerInfo> util = new ExcelUtil<BusCustomerInfo>(BusCustomerInfo.class);
        util.exportExcel(response, list, "客户档案数据");
    }

    /**
     * 获取客户档案详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:customerInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busCustomerInfoService.selectBusCustomerInfoById(id));
    }

    /**
     * 新增客户档案
     */
    @PreAuthorize("@ss.hasPermi('business:customerInfo:add')")
    @Log(title = "客户档案", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusCustomerInfoVO busCustomerInfo)
    {
        return toAjax(busCustomerInfoService.insertBusCustomerInfo(busCustomerInfo));
    }

    /**
     * 修改客户档案
     */
    @PreAuthorize("@ss.hasPermi('business:customerInfo:edit')")
    @Log(title = "客户档案", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusCustomerInfoVO busCustomerInfo)
    {
        return toAjax(busCustomerInfoService.updateBusCustomerInfo(busCustomerInfo));
    }

    /**
     * 删除客户档案
     */
    @PreAuthorize("@ss.hasPermi('business:customerInfo:remove')")
    @Log(title = "客户档案", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busCustomerInfoService.deleteBusCustomerInfoByIds(ids));
    }
}
