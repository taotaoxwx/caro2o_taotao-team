package cn.wolfcode.business.bpmninfo.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusBpmnInfoVO
{
    /** 流程名称 */
    private String bpmnLabel;
    /** 审核类型 */
    private Integer bpmnType;
    /** 描述信息 */
    @Excel(name = "描述信息")
    private String info;

}
