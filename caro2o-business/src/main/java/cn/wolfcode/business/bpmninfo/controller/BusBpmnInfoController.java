package cn.wolfcode.business.bpmninfo.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.bpmninfo.domain.vo.BusBpmnInfoVO;
import org.apache.commons.io.IOUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.bpmninfo.domain.BusBpmnInfo;
import cn.wolfcode.business.bpmninfo.service.IBusBpmnInfoService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 流程定义明细Controller
 * 
 * @author wolfcode
 * @date 2023-06-03
 */
@RestController
@RequestMapping("/business/bpmninfo")
public class BusBpmnInfoController extends BaseController
{
    @Autowired
    private IBusBpmnInfoService busBpmnInfoService;

    /**
     * 查询流程定义明细列表
     */
    @PreAuthorize("@ss.hasPermi('business:bpmninfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusBpmnInfo busBpmnInfo)
    {
        startPage();
        List<BusBpmnInfo> list = busBpmnInfoService.selectBusBpmnInfoList(busBpmnInfo);
        return getDataTable(list);
    }

    /**
     * 获取流程定义明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:bpmninfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busBpmnInfoService.selectBusBpmnInfoById(id));
    }

    /**
     * 撤销流程定义明细
     */
    @PreAuthorize("@ss.hasPermi('business:bpmninfo:cancel')")
	@PostMapping("/cancel/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(busBpmnInfoService.deleteBusBpmnInfoById(id));
    }

    /**
     * 部署流程定义
     */
    @PreAuthorize("@ss.hasPermi('business:bpmninfo:deploy')")
	@PostMapping("/deploy")
    public AjaxResult deploy(MultipartFile file, BusBpmnInfoVO busBpmnInfoVO) throws IOException {
        busBpmnInfoService.deploy(file,busBpmnInfoVO);
        return AjaxResult.success();
    }

    /**
     * 查看流程定义明细
     */
    @PreAuthorize("@ss.hasPermi('business:bpmninfo:process')")
    @GetMapping("/getProcess/{id}/{type}")
    public void getProcess(@PathVariable Long id ,@PathVariable String type, HttpServletResponse response) throws IOException {
        InputStream inputStream = busBpmnInfoService.getProcess(id , type);
        IOUtils.copy(inputStream, response.getOutputStream());
    }
}
