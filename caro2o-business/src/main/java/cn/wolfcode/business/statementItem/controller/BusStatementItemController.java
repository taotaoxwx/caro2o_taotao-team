package cn.wolfcode.business.statementItem.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.statementItem.domain.vo.BusStatementItemVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.statementItem.domain.BusStatementItem;
import cn.wolfcode.business.statementItem.service.IBusStatementItemService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 结算单明细Controller
 * 
 * @author wolfcode
 * @date 2023-05-31
 */
@RestController
@RequestMapping("/business/statementItem")
public class BusStatementItemController extends BaseController
{
    @Autowired
    private IBusStatementItemService busStatementItemService;

    /**
     * 查询结算单明细列表
     */
    @PreAuthorize("@ss.hasPermi('business:statementItem:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusStatementItem busStatementItem)
    {
        startPage();
        List<BusStatementItem> list = busStatementItemService.selectBusStatementItemList(busStatementItem);
        return getDataTable(list);
    }

//    /**
//     * 获取结算单明细详细信息
//     */
//    @PreAuthorize("@ss.hasPermi(' ')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return AjaxResult.success(busStatementItemService.selectBusStatementItemById(id));
//    }

    /**
     * 新增结算单明细
     */
    @PreAuthorize("@ss.hasPermi('business:statementItem:add')")
    @PostMapping
    public AjaxResult add(@RequestBody BusStatementItemVO busStatementItemVO)
    {
        return toAjax(busStatementItemService.insertBusStatementItem(busStatementItemVO));
    }

    /**
     * 修改结算单明细
     */
    @PreAuthorize("@ss.hasPermi('business:statementItem:pay')")
    @PostMapping("/pay/{id}")
    public AjaxResult pay(@PathVariable Long id)
    {
        return toAjax(busStatementItemService.pay(id));
    }

//    /**
//     * 删除结算单明细
//     */
//    @PreAuthorize("@ss.hasPermi('business:statementItem:remove')")
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(busStatementItemService.deleteBusStatementItemByIds(ids));
//    }
}
