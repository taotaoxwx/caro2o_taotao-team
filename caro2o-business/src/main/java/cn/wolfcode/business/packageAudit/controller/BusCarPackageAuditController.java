package cn.wolfcode.business.packageAudit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.packageAudit.domain.vo.BusAuditHistory;
import cn.wolfcode.business.packageAudit.domain.vo.BusCarPackageAuditVO;
import org.apache.commons.io.IOUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.packageAudit.domain.BusCarPackageAudit;
import cn.wolfcode.business.packageAudit.service.IBusCarPackageAuditService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 套餐审核Controller
 * 
 * @author wolfcode
 * @date 2023-06-04
 */
@RestController
@RequestMapping("/business/packageAudit")
public class BusCarPackageAuditController extends BaseController
{
    @Autowired
    private IBusCarPackageAuditService busCarPackageAuditService;

    /**
     * 查询套餐审核列表
     */
    @PreAuthorize("@ss.hasPermi('business:packageAudit:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusCarPackageAudit busCarPackageAudit)
    {
        startPage();
        List<BusCarPackageAudit> list = busCarPackageAuditService.selectBusCarPackageAuditList(busCarPackageAudit);
        return getDataTable(list);
    }
    /**
     * 查询代办套餐审核列表
     */
    @PreAuthorize("@ss.hasPermi('business:packageAudit:myList')")
    @GetMapping("/myList")
    public TableDataInfo myList(BusCarPackageAudit busCarPackageAudit)
    {
        startPage();
        List<BusCarPackageAudit> list = busCarPackageAuditService.selectMyBusCarPackageAuditList(busCarPackageAudit);
        return getDataTable(list);
    }
    /**
     * 查询已办套餐审核列表
     */
    @PreAuthorize("@ss.hasPermi('business:packageAudit:myDoneList')")
    @GetMapping("/myDoneList")
    public TableDataInfo myDoneList(BusCarPackageAudit busCarPackageAudit)
    {
        startPage();
        List<BusCarPackageAudit> list = busCarPackageAuditService.selectmyDoneList(busCarPackageAudit);
        return getDataTable(list);
    }


    /**
     * 获取套餐审核详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:packageAudit:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busCarPackageAuditService.selectBusCarPackageAuditById(id));
    }

    /**
     * 新增套餐审核
     */
    @PreAuthorize("@ss.hasPermi('business:packageAudit:add')")
    @PostMapping
    public AjaxResult add(@RequestBody BusCarPackageAudit busCarPackageAudit)
    {
        return toAjax(busCarPackageAuditService.insertBusCarPackageAudit(busCarPackageAudit));
    }

    /**
     * 修改套餐审核
     */
    @PreAuthorize("@ss.hasPermi('business:packageAudit:edit')")
    @PutMapping
    public AjaxResult edit(@RequestBody BusCarPackageAudit busCarPackageAudit)
    {
        return toAjax(busCarPackageAuditService.updateBusCarPackageAudit(busCarPackageAudit));
    }

    /**
     * 删除套餐审核
     */
    @PreAuthorize("@ss.hasPermi('business:packageAudit:cancel')")
	@PostMapping("/cancel/{id}")
    public AjaxResult cancel(@PathVariable Long id)
    {
        busCarPackageAuditService.deleteBusCarPackageAuditById(id);
        return AjaxResult.success();
    }

    /**
     * 查看流程定义明细
     */
    @PreAuthorize("@ss.hasPermi('business:packageAudit:auditProcess')")
    @GetMapping("/getAuditProcess/{id}")
    public void getAuditProcess(@PathVariable Long id , HttpServletResponse response) throws IOException {
        InputStream inputStream = busCarPackageAuditService.getAuditProcess(id);
        IOUtils.copy(inputStream,response.getOutputStream());
    }

    @PreAuthorize("@ss.hasPermi('business:packageAudit:myAudit')")
    @PostMapping("/auditMyTodo")
    public AjaxResult auditMyTodo(@RequestBody BusCarPackageAuditVO busCarPackageAuditVO){
        busCarPackageAuditService.auditTask(busCarPackageAuditVO);
        return AjaxResult.success();
    }

    /**
     * 查看流程定义明细
     */
    @PreAuthorize("@ss.hasPermi('business:packageAudit:auditHistory')")
    @GetMapping("/getAuditHistory/{id}")
    public TableDataInfo getAuditHistory(@PathVariable Long id) {
        startPage();
        List<BusAuditHistory> list = busCarPackageAuditService.getBusAuditHistoryByBusPackageAuditId(id);
        return getDataTable(list);
    }
}
