package cn.wolfcode.business.appointment.service;

import java.util.List;
import cn.wolfcode.business.appointment.domain.BusAppointment;
import cn.wolfcode.business.appointment.domain.vo.BusAppointmentVO;

/**
 * 养修信息预约Service接口
 * 
 * @author wolfcode
 * @date 2023-05-28
 */
public interface IBusAppointmentService 
{
    /**
     * 查询养修信息预约
     * 
     * @param id 养修信息预约主键
     * @return 养修信息预约
     */
    public BusAppointment selectBusAppointmentById(Long id);

    /**
     * 查询养修信息预约列表
     * 
     * @param busAppointment 养修信息预约
     * @return 养修信息预约集合
     */
    public List<BusAppointment> selectBusAppointmentList(BusAppointment busAppointment);

    /**
     * 新增养修信息预约
     * 
     * @param busAppointmentVO 养修信息预约
     * @return 结果
     */
    public int insertBusAppointment(BusAppointmentVO busAppointmentVO);

    /**
     * 修改养修信息预约
     * 
     * @param busAppointmentVO 养修信息预约
     * @return 结果
     */
    public int updateBusAppointment(BusAppointmentVO busAppointmentVO);

    /**
     * 批量删除养修信息预约
     * 
     * @param ids 需要删除的养修信息预约主键集合
     * @return 结果
     */
    public int deleteBusAppointmentByIds(Long[] ids);

    /**
     * 删除养修信息预约信息
     * 
     * @param id 养修信息预约主键
     * @return 结果
     */
    public int deleteBusAppointmentById(Long id);

    /**
     * 修改到店状态预约
     *
     * @param id 需要修改到店状态的主键
     * @return 结果
     */
    public int changeArriveShopStatus(Long id);

    public int cancelStatus(Long id);

    public Long getBusStatementIdByAppointmentSettle(Long id);

    public void overtimeStatus();

    public int synMsg(Long id);
}
