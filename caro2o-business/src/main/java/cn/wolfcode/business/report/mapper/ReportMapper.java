package cn.wolfcode.business.report.mapper;

import cn.wolfcode.business.report.domain.vo.CustomerReportQO;
import cn.wolfcode.business.report.domain.vo.CustomerReportVO;
import cn.wolfcode.business.report.domain.vo.ShopReportQO;
import cn.wolfcode.business.report.domain.vo.ShopReportVO;

import java.util.List;

public interface ReportMapper {

    public List<ShopReportVO> getShopReport(ShopReportQO qo);

    public List<CustomerReportVO> getCustomerReport(CustomerReportQO qo);
}
