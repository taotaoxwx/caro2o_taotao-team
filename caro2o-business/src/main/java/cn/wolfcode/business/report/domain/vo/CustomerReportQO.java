package cn.wolfcode.business.report.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CustomerReportQO {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    public Date getEndTime(){
        Calendar calendar = Calendar.getInstance();
        if(this.endTime != null){
            calendar.setTime(this.endTime);
            calendar.add(Calendar.DAY_OF_MONTH,1);
            return calendar.getTime();
        }
        return null;
    }
}
