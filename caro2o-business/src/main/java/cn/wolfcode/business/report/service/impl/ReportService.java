package cn.wolfcode.business.report.service.impl;

import cn.wolfcode.business.report.domain.vo.CustomerReportQO;
import cn.wolfcode.business.report.domain.vo.CustomerReportVO;
import cn.wolfcode.business.report.domain.vo.ShopReportQO;
import cn.wolfcode.business.report.domain.vo.ShopReportVO;
import cn.wolfcode.business.report.mapper.ReportMapper;
import cn.wolfcode.business.report.service.IReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService implements IReportService {
    @Autowired
    private ReportMapper reportMapper;
    @Override
    public List<ShopReportVO> queryShopReport(ShopReportQO qo) {
        return reportMapper.getShopReport(qo);
    }

    @Override
    public List<CustomerReportVO> queryCustomerReport(CustomerReportQO qo) {
        return reportMapper.getCustomerReport(qo);
    }
}
