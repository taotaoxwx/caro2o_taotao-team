package cn.wolfcode.business.report.controller;

import cn.wolfcode.business.appointment.domain.BusAppointment;
import cn.wolfcode.business.report.domain.vo.CustomerReportQO;
import cn.wolfcode.business.report.domain.vo.CustomerReportVO;
import cn.wolfcode.business.report.domain.vo.ShopReportQO;
import cn.wolfcode.business.report.domain.vo.ShopReportVO;
import cn.wolfcode.business.report.service.IReportService;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static cn.wolfcode.common.utils.PageUtils.startPage;

@RestController
@RequestMapping("/report")
public class ReportController extends BaseController {
    @Autowired
    private IReportService reportService;

    /**
     * 查询养修信息预约列表
     */
//    @PreAuthorize("@ss.hasPermi('business:appointment:list')")
    @GetMapping("/shop")
    public TableDataInfo shopList(ShopReportQO qo) {
        startPage();
        List<ShopReportVO> list = reportService.queryShopReport(qo);
        return getDataTable(list);
    }
    /**
     * 查询养修信息预约列表
     */
//    @PreAuthorize("@ss.hasPermi('business:appointment:list')")
    @GetMapping("/customer")
    public TableDataInfo customerList(CustomerReportQO qo) {
        startPage();
        List<CustomerReportVO> list = reportService.queryCustomerReport(qo);
        return getDataTable(list);
    }
}
