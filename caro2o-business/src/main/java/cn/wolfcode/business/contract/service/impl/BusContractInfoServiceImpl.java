package cn.wolfcode.business.contract.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import cn.wolfcode.business.contract.domain.vo.BusContractVO;
import cn.wolfcode.common.config.RuoYiConfig;
import cn.wolfcode.common.utils.DateUtils;
import cn.wolfcode.common.utils.SecurityUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.contract.mapper.BusContractInfoMapper;
import cn.wolfcode.business.contract.domain.BusContractInfo;
import cn.wolfcode.business.contract.service.IBusContractInfoService;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

/**
 * 合同管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-06-17
 */
@Service
public class BusContractInfoServiceImpl implements IBusContractInfoService 
{
    @Autowired
    private BusContractInfoMapper busContractInfoMapper;

    /**
     * 查询合同管理
     * 
     * @param id 合同管理主键
     * @return 合同管理
     */
    @Override
    public BusContractInfo selectBusContractInfoById(Long id)
    {
        return busContractInfoMapper.selectBusContractInfoById(id);
    }

    /**
     * 查询合同管理列表
     * 
     * @param busContractInfo 合同管理
     * @return 合同管理
     */
    @Override
    public List<BusContractInfo> selectBusContractInfoList(BusContractInfo busContractInfo)
    {
        List<BusContractInfo> busContractInfos = busContractInfoMapper.selectBusContractInfoList(busContractInfo);

        return busContractInfos;
    }

    /**
     * 新增合同管理
     *
     * @return 结果
     */
    @Override
    public void insertBusContractInfo(MultipartFile file,BusContractVO vo) throws IOException {
        Assert.notNull(vo,"参数异常");
        Assert.notNull(file,"文件不能为空");
        if (vo.getFileSize() > 2097152){
            throw new RuntimeException("只能上传2M以内的文件");
        }
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        BusContractInfo busContractInfo = new BusContractInfo();
        BeanUtils.copyProperties(vo,busContractInfo);
        busContractInfo.setFileName(vo.getContractName()+"电子版."+extension);
        busContractInfo.setEntryPerson(SecurityUtils.getUsername());
        busContractInfo.setEntryTime(new Date());
        busContractInfo.setStatus(BusContractInfo.AUDIt_STATUS_REVIEW);
        busContractInfo.setIsCancel(BusContractInfo.CANCEL_STATUS_FALSE);
        busContractInfo.setIsStamp(BusContractInfo.STAMP_STATUS_FALSE);
        busContractInfo.setContractAnnex(file.getBytes());

        busContractInfoMapper.insertBusContractInfo(busContractInfo);

    }

    /**
     * 修改合同管理
     * @return 结果
     */
    @Override
    public void updateBusContractInfo(MultipartFile file, BusContractVO vo) throws IOException {
        Assert.notNull(vo,"参数异常");


        BusContractInfo busContractInfo = busContractInfoMapper.selectBusContractInfoById(vo.getId());
        Assert.notNull(busContractInfo,"参数异常");
        if (BusContractInfo.AUDIt_STATUS_SUCCESS.equals(busContractInfo.getStatus())){
            throw new RuntimeException("审核通过状态无法修改");
        }
        BeanUtils.copyProperties(vo,busContractInfo);
        if (file != null){
            if (vo.getFileSize() > 2097152){
                throw new RuntimeException("只能上传2M以内的文件");
            }
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
            busContractInfo.setFileName(vo.getContractName()+"电子版."+extension);
            busContractInfo.setContractAnnex(file.getBytes());
        }
        busContractInfo.setUpdateTime(DateUtils.getNowDate());
        busContractInfo.setStatus(BusContractInfo.AUDIt_STATUS_REVIEW);
        busContractInfoMapper.updateBusContractInfo(busContractInfo);

    }

    /**
     * 批量删除合同管理
     * 
     * @param ids 需要删除的合同管理主键
     * @return 结果
     */
    @Override
    public int deleteBusContractInfoByIds(Long[] ids)
    {
        return busContractInfoMapper.deleteBusContractInfoByIds(ids);
    }

    /**
     * 删除合同管理信息
     * 
     * @param id 合同管理主键
     * @return 结果
     */
    @Override
    public void deleteBusContractInfoById(Long id)
    {
        Assert.notNull(id,"参数异常");
        BusContractInfo busContractInfo = busContractInfoMapper.selectBusContractInfoById(id);
        Assert.notNull(busContractInfo,"参数异常");
        Assert.state(BusContractInfo.STAMP_STATUS_FALSE.equals(busContractInfo.getIsStamp()),"只有未盖章的合同才能作废");
        Assert.state(BusContractInfo.CANCEL_STATUS_FALSE.equals(busContractInfo.getIsCancel()),"已作废的合同不能再次作废");
        busContractInfoMapper.updateCancelStatusById(id,BusContractInfo.CANCEL_STATUS_TRUE);
    }

    @Override
    public void updateStamp(Long id) {
        Assert.notNull(id,"参数异常");
        BusContractInfo busContractInfo = busContractInfoMapper.selectBusContractInfoById(id);
        Assert.notNull(busContractInfo,"参数异常");
        Assert.state(BusContractInfo.AUDIt_STATUS_SUCCESS.equals(busContractInfo.getStatus()),"只有审核通过的合同才能盖章");
        Assert.state(BusContractInfo.STAMP_STATUS_FALSE.equals(busContractInfo.getIsStamp()),"已盖章的合同不能再次盖章");
        busContractInfoMapper.updateStampStatusById(id,BusContractInfo.STAMP_STATUS_TRUE);
    }

    @Override
    public void updateStatusSuccess(Long id) {
        Assert.notNull(id,"参数异常");
        BusContractInfo busContractInfo = busContractInfoMapper.selectBusContractInfoById(id);
        Assert.notNull(busContractInfo,"参数异常");
        Assert.state(BusContractInfo.AUDIt_STATUS_REVIEW.equals(busContractInfo.getStatus()),"只有待审核的合同才能进行审核");
        busContractInfoMapper.updateAuditStatusById(id,BusContractInfo.AUDIt_STATUS_SUCCESS);
    }

    @Override
    public void updateStatusFailed(Long id) {
        Assert.notNull(id,"参数异常");
        BusContractInfo busContractInfo = busContractInfoMapper.selectBusContractInfoById(id);
        Assert.notNull(busContractInfo,"参数异常");
        Assert.state(BusContractInfo.AUDIt_STATUS_REVIEW.equals(busContractInfo.getStatus()),"只有待审核的合同才能进行审核");
        busContractInfoMapper.updateAuditStatusById(id,BusContractInfo.AUDIt_STATUS_FAILED);

    }



}
