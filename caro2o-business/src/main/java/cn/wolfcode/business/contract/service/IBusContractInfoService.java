package cn.wolfcode.business.contract.service;

import java.io.IOException;
import java.util.List;
import cn.wolfcode.business.contract.domain.BusContractInfo;
import cn.wolfcode.business.contract.domain.vo.BusContractVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * 合同管理Service接口
 * 
 * @author ruoyi
 * @date 2023-06-17
 */
public interface IBusContractInfoService 
{
    /**
     * 查询合同管理
     * 
     * @param id 合同管理主键
     * @return 合同管理
     */
    public BusContractInfo selectBusContractInfoById(Long id);

    /**
     * 查询合同管理列表
     * 
     * @param busContractInfo 合同管理
     * @return 合同管理集合
     */
    public List<BusContractInfo> selectBusContractInfoList(BusContractInfo busContractInfo);

    /**
     * 新增合同管理
     *
     * @return 结果
     */
    public void insertBusContractInfo(MultipartFile file, BusContractVO vo) throws IOException;

    /**
     * 修改合同管理
     * 
     * @param busContractInfo 合同管理
     */
    public void updateBusContractInfo(MultipartFile file, BusContractVO busContractInfo) throws IOException;

    /**
     * 批量删除合同管理
     * 
     * @param ids 需要删除的合同管理主键集合
     * @return 结果
     */
    public int deleteBusContractInfoByIds(Long[] ids);

    /**
     * 作废合同管理信息
     * 
     * @param id 合同管理主键
     * @return 结果
     */
    public void deleteBusContractInfoById(Long id);

    void updateStamp(Long id);

    void updateStatusSuccess(Long id);

    void updateStatusFailed(Long id);
}
