package cn.wolfcode.business.contacts.service.impl;

import java.util.Date;
import java.util.List;

import cn.wolfcode.business.appointment.util.RegexUtils;
import cn.wolfcode.business.contacts.domain.vo.EnterpriseContactsQO;
import cn.wolfcode.business.contacts.domain.vo.EnterpriseContactsVO;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;
import cn.wolfcode.business.enterpriseInfo.mapper.EnterpriseInfoMapper;
import cn.wolfcode.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.contacts.mapper.EnterpriseContactsMapper;
import cn.wolfcode.business.contacts.domain.EnterpriseContacts;
import cn.wolfcode.business.contacts.service.IEnterpriseContactsService;
import org.springframework.util.Assert;

/**
 * 联系人Service业务层处理
 * 
 * @author 韬韬
 * @date 2023-06-17
 */
@Service
public class EnterpriseContactsServiceImpl implements IEnterpriseContactsService 
{
    @Autowired
    private EnterpriseContactsMapper enterpriseContactsMapper;
    @Autowired
    private EnterpriseInfoMapper enterpriseInfoMapper;

    /**
     * 查询联系人
     * 
     * @param id 联系人主键
     * @return 联系人
     */
    @Override
    public EnterpriseContacts selectEnterpriseContactsById(Long id)
    {
        return enterpriseContactsMapper.selectEnterpriseContactsById(id);
    }

    /**
     * 查询联系人列表
     * 
     * @param enterpriseContacts 联系人
     * @return 联系人
     */
    @Override
    public List<EnterpriseContacts> selectEnterpriseContactsList(EnterpriseContactsQO enterpriseContacts)
    {
        return enterpriseContactsMapper.selectEnterpriseContactsList(enterpriseContacts);
    }

    /**
     * 新增联系人
     * 
     * @param enterpriseContactsVO 联系人
     * @return 结果
     */
    @Override
    public int insertEnterpriseContacts(EnterpriseContactsVO enterpriseContactsVO)
    {
        Assert.notNull(enterpriseContactsVO,"非法操作");
        Assert.notNull(enterpriseContactsVO.getEnterpriseName(),"企业名称不能为空");
        Assert.notNull(enterpriseContactsVO.getContacts(),"联系人名字不能为空");
        Assert.notNull(enterpriseContactsVO.getGender(),"性别不能为空");
        Assert.notNull(enterpriseContactsVO.getAge(),"年龄不能为空");
        Assert.notNull(enterpriseContactsVO.getContactPhone(),"联系电话不能为空");
        Assert.state(RegexUtils.isPhoneLegal(enterpriseContactsVO.getContactPhone()),"联系电话输入有误");
        Assert.notNull(enterpriseContactsVO.getPosition(),"职位不能为空");
        Assert.notNull(enterpriseContactsVO.getDepartment(),"部门不能为空");
        EnterpriseContacts enterpriseContacts = new EnterpriseContacts();
        BeanUtils.copyProperties(enterpriseContactsVO,enterpriseContacts);
        enterpriseContacts.setEnploymentStatus(EnterpriseContacts.ENPLOYMENNT_STATUS_ON);
        enterpriseContacts.setClerk(SecurityUtils.getUsername());
        enterpriseContacts.setEntryTime(new Date());
        return enterpriseContactsMapper.insertEnterpriseContacts(enterpriseContacts);
    }

    /**
     * 修改联系人
     * 
     * @param enterpriseContactsVO 联系人
     * @return 结果
     */
    @Override
    public int updateEnterpriseContacts(EnterpriseContactsVO enterpriseContactsVO)
    {
        Assert.notNull(enterpriseContactsVO,"非法操作");
        Assert.notNull(enterpriseContactsVO.getEnterpriseName(),"企业名称不能为空");
        Assert.notNull(enterpriseContactsVO.getContacts(),"联系人名字不能为空");
        Assert.notNull(enterpriseContactsVO.getGender(),"性别不能为空");
        Assert.notNull(enterpriseContactsVO.getAge(),"年龄不能为空");
        Assert.notNull(enterpriseContactsVO.getContactPhone(),"联系电话不能为空");
        Assert.state(RegexUtils.isPhoneLegal(enterpriseContactsVO.getContactPhone()),"联系电话输入有误");
        Assert.notNull(enterpriseContactsVO.getPosition(),"职位不能为空");
        Assert.notNull(enterpriseContactsVO.getDepartment(),"部门不能为空");
        Assert.notNull(enterpriseContactsVO.getEnploymentStatus(),"任职状态不能为空");
        EnterpriseContacts enterpriseContacts = enterpriseContactsMapper.selectEnterpriseContactsById(enterpriseContactsVO.getId());
        Assert.notNull(enterpriseContacts,"非法操作");
        BeanUtils.copyProperties(enterpriseContactsVO,enterpriseContacts);
        return enterpriseContactsMapper.updateEnterpriseContacts(enterpriseContacts);
    }

    /**
     * 批量删除联系人
     * 
     * @param ids 需要删除的联系人主键
     * @return 结果
     */
    @Override
    public int deleteEnterpriseContactsByIds(Long[] ids)
    {
        return enterpriseContactsMapper.deleteEnterpriseContactsByIds(ids);
    }

    /**
     * 删除联系人信息
     * 
     * @param id 联系人主键
     * @return 结果
     */
    @Override
    public int deleteEnterpriseContactsById(Long id)
    {
        return enterpriseContactsMapper.deleteEnterpriseContactsById(id);
    }

    @Override
    public List<EnterpriseInfo> getEnterpriseNameList() {
        return enterpriseInfoMapper.getEnterpriseNameList();
    }

    @Override
    public List<EnterpriseContacts> getContectsList() {
        return enterpriseContactsMapper.getContectsList();
    }
}
