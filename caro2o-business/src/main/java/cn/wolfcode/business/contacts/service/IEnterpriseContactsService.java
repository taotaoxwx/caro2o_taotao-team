package cn.wolfcode.business.contacts.service;

import java.util.List;
import cn.wolfcode.business.contacts.domain.EnterpriseContacts;
import cn.wolfcode.business.contacts.domain.vo.EnterpriseContactsQO;
import cn.wolfcode.business.contacts.domain.vo.EnterpriseContactsVO;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;

/**
 * 联系人Service接口
 * 
 * @author 韬韬
 * @date 2023-06-17
 */
public interface IEnterpriseContactsService 
{
    /**
     * 查询联系人
     * 
     * @param id 联系人主键
     * @return 联系人
     */
    public EnterpriseContacts selectEnterpriseContactsById(Long id);

    /**
     * 查询联系人列表
     * 
     * @param enterpriseContacts 联系人
     * @return 联系人集合
     */
    public List<EnterpriseContacts> selectEnterpriseContactsList(EnterpriseContactsQO enterpriseContacts);

    /**
     * 新增联系人
     * 
     * @param enterpriseContacts 联系人
     * @return 结果
     */
    public int insertEnterpriseContacts(EnterpriseContactsVO enterpriseContacts);

    /**
     * 修改联系人
     * 
     * @param enterpriseContacts 联系人
     * @return 结果
     */
    public int updateEnterpriseContacts(EnterpriseContactsVO enterpriseContacts);

    /**
     * 批量删除联系人
     * 
     * @param ids 需要删除的联系人主键集合
     * @return 结果
     */
    public int deleteEnterpriseContactsByIds(Long[] ids);

    /**
     * 删除联系人信息
     * 
     * @param id 联系人主键
     * @return 结果
     */
    public int deleteEnterpriseContactsById(Long id);

    public List<EnterpriseInfo> getEnterpriseNameList();


    public List<EnterpriseContacts> getContectsList();

}
