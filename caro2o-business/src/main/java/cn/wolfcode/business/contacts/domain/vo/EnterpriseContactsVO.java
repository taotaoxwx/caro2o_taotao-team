package cn.wolfcode.business.contacts.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 联系人对象 enterprise_contacts
 * 
 * @author 韬韬
 * @date 2023-06-17
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class EnterpriseContactsVO extends BaseEntity
{
    /** $column.columnComment */
    private Long id;

    /** 企业名称 */
    @Excel(name = "企业名称")
    private String enterpriseName;
    /** 联系人名字 */
    @Excel(name = "联系人名字")
    private String contacts;
    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;
    /** 年龄 */
    @Excel(name = "年龄")
    private Integer age;
    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contactPhone;
    /** 职位 */
    @Excel(name = "职位")
    private String position;
    /** 部门 */
    @Excel(name = "部门")
    private String department;
    /** 任职状态 */
    @Excel(name = "任职状态")
    private Integer enploymentStatus;
}
