package cn.wolfcode.business.contacts.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 联系人对象 enterprise_contacts
 * 
 * @author 韬韬
 * @date 2023-06-17
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class EnterpriseContactsQO extends BaseEntity
{
    private Long id;

    @Excel(name = "企业名称")
    private String enterpriseName;

    private String keyword;

    /** 任职状态 */
    @Excel(name = "任职状态")
    private Integer enploymentStatus;
}
