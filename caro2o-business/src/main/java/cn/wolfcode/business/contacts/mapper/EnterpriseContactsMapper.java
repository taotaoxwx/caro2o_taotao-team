package cn.wolfcode.business.contacts.mapper;

import java.util.List;
import cn.wolfcode.business.contacts.domain.EnterpriseContacts;
import cn.wolfcode.business.contacts.domain.vo.EnterpriseContactsQO;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;

/**
 * 联系人Mapper接口
 * 
 * @author 韬韬
 * @date 2023-06-17
 */
public interface EnterpriseContactsMapper 
{
    /**
     * 查询联系人
     * 
     * @param id 联系人主键
     * @return 联系人
     */
    public EnterpriseContacts selectEnterpriseContactsById(Long id);

    /**
     * 查询联系人列表
     * 
     * @param enterpriseContacts 联系人
     * @return 联系人集合
     */
    public List<EnterpriseContacts> selectEnterpriseContactsList(EnterpriseContactsQO enterpriseContacts);

    /**
     * 新增联系人
     * 
     * @param enterpriseContacts 联系人
     * @return 结果
     */
    public int insertEnterpriseContacts(EnterpriseContacts enterpriseContacts);

    /**
     * 修改联系人
     * 
     * @param enterpriseContacts 联系人
     * @return 结果
     */
    public int updateEnterpriseContacts(EnterpriseContacts enterpriseContacts);

    /**
     * 删除联系人
     * 
     * @param id 联系人主键
     * @return 结果
     */
    public int deleteEnterpriseContactsById(Long id);

    /**
     * 批量删除联系人
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEnterpriseContactsByIds(Long[] ids);


    public List<EnterpriseContacts> getContectsList();

}
