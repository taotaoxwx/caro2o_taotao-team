package cn.wolfcode.business.enterpriseInfo.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.business.enterpriseInfo.domain.qo.EnterpriseInfoQO;
import cn.wolfcode.business.enterpriseInfo.domain.qo.EnterpriseInfoVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;
import cn.wolfcode.business.enterpriseInfo.service.IEnterpriseInfoService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 客户基本信息Controller
 * 
 * @author taotao
 * @date 2023-06-16
 */
@RestController
@RequestMapping("/business/enterpriseInfo")
public class EnterpriseInfoController extends BaseController
{
    @Autowired
    private IEnterpriseInfoService enterpriseInfoService;

    /**
     * 查询客户基本信息列表
     */
    @PreAuthorize("@ss.hasPermi('business:enterpriseInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(EnterpriseInfoQO enterpriseInfo)
    {
        startPage();
        List<EnterpriseInfo> list = enterpriseInfoService.selectEnterpriseInfoList(enterpriseInfo);
        return getDataTable(list);
    }

    /**
     * 获取客户基本信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:enterpriseInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(enterpriseInfoService.selectEnterpriseInfoById(id));
    }

    @PreAuthorize("@ss.hasPermi('business:enterpriseInfo:details')")
    @GetMapping(value = "/details/{id}")
    public TableDataInfo getDetailsInfo(@PathVariable("id") Long id)
    {
        startPage();
        List<EnterpriseInfo> list = enterpriseInfoService.getEnterpriseDetailsById(id);
        return getDataTable(list);
    }

    /**
     * 新增客户基本信息
     */
    @PreAuthorize("@ss.hasPermi('business:enterpriseInfo:add')")
    @Log(title = "客户基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EnterpriseInfoVO enterpriseInfo)
    {
        return toAjax(enterpriseInfoService.insertEnterpriseInfo(enterpriseInfo));
    }

    /**
     * 修改客户基本信息
     */
    @PreAuthorize("@ss.hasPermi('business:enterpriseInfo:edit')")
    @Log(title = "客户基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EnterpriseInfoVO enterpriseInfo)
    {
        return toAjax(enterpriseInfoService.updateEnterpriseInfo(enterpriseInfo));
    }

    /**
     * 删除客户基本信息
     */
//    @PreAuthorize("@ss.hasPermi('business:enterpriseInfo:remove')")
//    @Log(title = "客户基本信息", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(enterpriseInfoService.deleteEnterpriseInfoByIds(ids));
//    }


    //查询所有用户信息
    @PreAuthorize("@ss.hasPermi('business:enterpriseInfo:list')")
    @GetMapping("/allList")
    public TableDataInfo allList()
    {
        List<EnterpriseInfo> list = enterpriseInfoService.selectEnterpriseInfoAllList();
        return getDataTable(list);
    }
}
