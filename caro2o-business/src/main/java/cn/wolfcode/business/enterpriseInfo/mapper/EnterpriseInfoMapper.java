package cn.wolfcode.business.enterpriseInfo.mapper;

import java.util.List;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;
import cn.wolfcode.business.enterpriseInfo.domain.qo.EnterpriseInfoQO;

/**
 * 客户基本信息Mapper接口
 * 
 * @author taotao
 * @date 2023-06-16
 */
public interface EnterpriseInfoMapper 
{
    /**
     * 查询客户基本信息
     * 
     * @param id 客户基本信息主键
     * @return 客户基本信息
     */
    public EnterpriseInfo selectEnterpriseInfoById(Long id);

    /**
     * 查询客户基本信息列表
     * 
     * @param enterpriseInfo 客户基本信息
     * @return 客户基本信息集合
     */
    public List<EnterpriseInfo> selectEnterpriseInfoList(EnterpriseInfoQO enterpriseInfo);

    /**
     * 新增客户基本信息
     * 
     * @param enterpriseInfo 客户基本信息
     * @return 结果
     */
    public int insertEnterpriseInfo(EnterpriseInfo enterpriseInfo);

    /**
     * 修改客户基本信息
     * 
     * @param enterpriseInfo 客户基本信息
     * @return 结果
     */
    public int updateEnterpriseInfo(EnterpriseInfo enterpriseInfo);

    /**
     * 删除客户基本信息
     * 
     * @param id 客户基本信息主键
     * @return 结果
     */
    public int deleteEnterpriseInfoById(Long id);

    /**
     * 批量删除客户基本信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEnterpriseInfoByIds(Long[] ids);

    public List<EnterpriseInfo> getEnterpriseNameList();

    public List<EnterpriseInfo> selectEnterpriseInfoAllList();

    public EnterpriseInfo getInfoByEnterpriseName(String enterpriseName);
}
