package cn.wolfcode.business.enterpriseInfo.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

/**
 * 客户基本信息对象 enterprise_info
 * 
 * @author taotao
 * @date 2023-06-16
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class EnterpriseInfo extends BaseEntity
{
    public static final Integer OPERATING_STATUS_NORMAL = 0;//开业
    public static final Integer OPERATING_STATUS_CANCEL = 1;//注销
    public static final Integer OPERATING_STATUS_BROKE = 2;//破产
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 企业名称 */
    @Excel(name = "企业名称")
    private String enterpriseName;

    /** 法定代表人 */
    @Excel(name = "法定代表人")
    private String representative;

    /** 成立日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "成立日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date establishmentTime;

    /** 所属行业 */
    @Excel(name = "所属行业")
    private String industryInvolved;

    /** 所属地区 */
    @Excel(name = "所属地区")
    private Integer affiliatingArea;

    /** 经营状态 */
    @Excel(name = "经营状态")
    private Integer operatingStatus;

    /** 录入人 */
    @Excel(name = "录入人")
    private String clerk;

    /** 营销人 */
    @Excel(name = "营销人")
    private String matchMan;

    /** 录入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date entryTime;

    /** 注册资本 */
    private BigDecimal registeredCapital;

    /** 注册地址 */
    private String registeredAddress;

    /** 经营范围 */
    private String businessScope;
}
