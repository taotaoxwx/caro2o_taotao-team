package cn.wolfcode.business.enterpriseInfo.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.wolfcode.business.contacts.domain.EnterpriseContacts;
import cn.wolfcode.business.contacts.mapper.EnterpriseContactsMapper;
import cn.wolfcode.business.enterpriseInfo.domain.qo.EnterpriseInfoQO;
import cn.wolfcode.business.enterpriseInfo.domain.qo.EnterpriseInfoVO;
import cn.wolfcode.business.enterpriseInfo.service.IEnterpriseInfoService;
import cn.wolfcode.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.enterpriseInfo.mapper.EnterpriseInfoMapper;
import cn.wolfcode.business.enterpriseInfo.domain.EnterpriseInfo;
import org.springframework.util.Assert;

/**
 * 客户基本信息Service业务层处理
 * 
 * @author taotao
 * @date 2023-06-16
 */
@Service
public class EnterpriseInfoServiceImpl implements IEnterpriseInfoService
{
    @Autowired
    private EnterpriseInfoMapper enterpriseInfoMapper;

    /**
     * 查询客户基本信息
     * 
     * @param id 客户基本信息主键
     * @return 客户基本信息
     */
    @Override
    public EnterpriseInfo selectEnterpriseInfoById(Long id)
    {
        return enterpriseInfoMapper.selectEnterpriseInfoById(id);
    }

    /**
     * 查询客户基本信息列表
     * 
     * @param enterpriseInfo 客户基本信息
     * @return 客户基本信息
     */
    @Override
    public List<EnterpriseInfo> selectEnterpriseInfoList(EnterpriseInfoQO enterpriseInfo)
    {
        return enterpriseInfoMapper.selectEnterpriseInfoList(enterpriseInfo);
    }

    /**
     * 新增客户基本信息
     * 
     * @param enterpriseInfoVO 客户基本信息
     * @return 结果
     */
    @Override
    public int insertEnterpriseInfo(EnterpriseInfoVO enterpriseInfoVO)
    {
        Assert.notNull(enterpriseInfoVO,"非法操作");
        Assert.notNull(enterpriseInfoVO.getEnterpriseName(),"企业名称不能为空");
        EnterpriseInfo enterpriseNameInfo = enterpriseInfoMapper.getInfoByEnterpriseName(enterpriseInfoVO.getEnterpriseName());
        Assert.isNull(enterpriseNameInfo,"企业名称已存在");

        Assert.notNull(enterpriseInfoVO.getRepresentative(),"法定代表人不能为空");
        Assert.notNull(enterpriseInfoVO.getEstablishmentTime(),"成立日期不能为空");
        Assert.notNull(enterpriseInfoVO.getAffiliatingArea(),"所属省份不能为空");
        Assert.notNull(enterpriseInfoVO.getRegisteredCapital(),"注册资本不能为空");
        Assert.state(enterpriseInfoVO.getRegisteredCapital().doubleValue() >= 0,"注册资本不能为空");
        Assert.notNull(enterpriseInfoVO.getRegisteredAddress(),"注册地址不能为空");
        Assert.notNull(enterpriseInfoVO.getIndustryInvolved(),"所属行业不能为空");
        Assert.notNull(enterpriseInfoVO.getBusinessScope(),"经营范围不能为空");
        EnterpriseInfo enterpriseInfo = new EnterpriseInfo();
        BeanUtils.copyProperties(enterpriseInfoVO,enterpriseInfo);
        enterpriseInfo.setOperatingStatus(EnterpriseInfo.OPERATING_STATUS_NORMAL);
        enterpriseInfo.setClerk(SecurityUtils.getUsername());
        enterpriseInfo.setMatchMan(SecurityUtils.getUsername());
        enterpriseInfo.setEntryTime(new Date());


        return enterpriseInfoMapper.insertEnterpriseInfo(enterpriseInfo);
    }

    /**
     * 修改客户基本信息
     * 
     * @param enterpriseInfoVO 客户基本信息
     * @return 结果
     */
    @Override
    public int updateEnterpriseInfo(EnterpriseInfoVO enterpriseInfoVO)
    {
        Assert.notNull(enterpriseInfoVO,"非法操作");
        Assert.notNull(enterpriseInfoVO.getEnterpriseName(),"企业名称不能为空");
        EnterpriseInfo enterpriseNameInfo = enterpriseInfoMapper.getInfoByEnterpriseName(enterpriseInfoVO.getEnterpriseName());
        Assert.notNull(enterpriseInfoVO.getRepresentative(),"法定代表人不能为空");
        Assert.notNull(enterpriseInfoVO.getEstablishmentTime(),"成立日期不能为空");
        Assert.notNull(enterpriseInfoVO.getOperatingStatus(),"经营状态不能为空");
        Assert.notNull(enterpriseInfoVO.getAffiliatingArea(),"所属省份不能为空");
        Assert.notNull(enterpriseInfoVO.getRegisteredCapital(),"注册资本不能为空");
        Assert.state(enterpriseInfoVO.getRegisteredCapital().doubleValue() >= 0,"注册资本不能为空");
        Assert.notNull(enterpriseInfoVO.getRegisteredAddress(),"注册地址不能为空");
        Assert.notNull(enterpriseInfoVO.getIndustryInvolved(),"所属行业不能为空");
        Assert.notNull(enterpriseInfoVO.getBusinessScope(),"经营范围不能为空");
        EnterpriseInfo enterpriseInfo = enterpriseInfoMapper.selectEnterpriseInfoById(enterpriseInfoVO.getId());
        Assert.notNull(enterpriseInfo,"非法操作");
        BeanUtils.copyProperties(enterpriseInfoVO,enterpriseInfo);
        return enterpriseInfoMapper.updateEnterpriseInfo(enterpriseInfo);
    }

    /**
     * 批量删除客户基本信息
     * 
     * @param ids 需要删除的客户基本信息主键
     * @return 结果
     */
    @Override
    public int deleteEnterpriseInfoByIds(Long[] ids)
    {
        return enterpriseInfoMapper.deleteEnterpriseInfoByIds(ids);
    }

    /**
     * 删除客户基本信息信息
     * 
     * @param id 客户基本信息主键
     * @return 结果
     */
    @Override
    public int deleteEnterpriseInfoById(Long id)
    {
        return enterpriseInfoMapper.deleteEnterpriseInfoById(id);
    }

    @Override
    public List<EnterpriseInfo> getEnterpriseDetailsById(Long id) {
        Assert.notNull(id,"非法操作");
        List<EnterpriseInfo> list= new ArrayList<>();
        EnterpriseInfo enterpriseInfo = enterpriseInfoMapper.selectEnterpriseInfoById(id);
        Assert.notNull(enterpriseInfo,"非法操作");
        list.add(enterpriseInfo);
        return list;
    }

    @Override
    public List<EnterpriseInfo> selectEnterpriseInfoAllList() {
        return enterpriseInfoMapper.selectEnterpriseInfoAllList();
    }
}
