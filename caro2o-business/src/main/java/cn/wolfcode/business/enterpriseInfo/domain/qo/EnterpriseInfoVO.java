package cn.wolfcode.business.enterpriseInfo.domain.qo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 客户基本信息对象 enterprise_info
 * 
 * @author taotao
 * @date 2023-06-16
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class EnterpriseInfoVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;
    @Excel(name = "企业名称")
    private String enterpriseName;
    @Excel(name = "法定代表人")
    private String representative;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "成立日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date establishmentTime;
    @Excel(name = "所属行业")
    private String industryInvolved;
    @Excel(name = "所属地区")
    private Integer affiliatingArea;
    @Excel(name = "经营状态")
    private Integer operatingStatus;
    /** 注册资本 */
    private BigDecimal registeredCapital;
    /** 注册地址 */
    private String registeredAddress;
    /** 经营范围 */
    private String businessScope;

}
