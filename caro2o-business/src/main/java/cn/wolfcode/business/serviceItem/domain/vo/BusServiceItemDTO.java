package cn.wolfcode.business.serviceItem.domain.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 服务项对象 bus_service_item
 * 
 * @author wolfcode
 * @date 2023-05-31
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusServiceItemDTO {
    private Long id;
    private Long shopOwnerId;
    private Long financeId;
    private String info;
}
