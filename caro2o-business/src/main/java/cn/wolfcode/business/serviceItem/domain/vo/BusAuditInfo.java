package cn.wolfcode.business.serviceItem.domain.vo;

import cn.wolfcode.business.serviceItem.domain.BusServiceItem;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import cn.wolfcode.common.core.domain.entity.SysUser;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务项对象 bus_service_item
 * 
 * @author wolfcode
 * @date 2023-05-31
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusAuditInfo {
    private BusServiceItem busServiceItem;
    private String info;
    private List<SysUser> shopOwners = new ArrayList<>();
    private List<SysUser> finances = new ArrayList<>();
}
