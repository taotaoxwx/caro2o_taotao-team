package cn.wolfcode.store.storePutOut.service;

import java.util.List;

import cn.wolfcode.business.warehouse.domain.BusWarehouseInfo;
import cn.wolfcode.store.storePutOut.domain.BusInventoryType;
import cn.wolfcode.store.storePutOut.vo.StoreItemVo;

/**
 * 出入库管理Service接口
 * 
 * @author chen
 * @date 2023-06-16
 */
public interface IBusInventoryTypeService 
{
    /**
     * 查询出入库管理
     * 
     * @param id 出入库管理主键
     * @return 出入库管理
     */
    public BusInventoryType selectBusInventoryTypeById(Long id);

    /**
     * 查询出入库管理列表
     * 
     * @param busInventoryType 出入库管理
     * @return 出入库管理集合
     */
    public List<BusInventoryType> selectBusInventoryTypeList(BusInventoryType busInventoryType);

    /**
     * 新增出入库管理
     * 
     * @param busInventoryType 出入库管理
     * @return 结果
     */
    public int insertBusInventoryType(BusInventoryType busInventoryType);

    /**
     * 修改出入库管理
     * 
     * @param busInventoryType 出入库管理
     * @return 结果
     */
    public int updateBusInventoryType(BusInventoryType busInventoryType);

    /**
     * 批量删除出入库管理
     * 
     * @param ids 需要删除的出入库管理主键集合
     * @return 结果
     */
    public int deleteBusInventoryTypeByIds(Long[] ids);

    /**
     * 删除出入库管理信息
     * 
     * @param id 出入库管理主键
     * @return 结果
     */
    public int deleteBusInventoryTypeById(Long id);

    /**
     * 查看仓库下拉框
     * @return
     */
    List<BusWarehouseInfo> listForStore();

    /**
     * 根据id查询单据明细
     * @param id
     * @return
     */
  StoreItemVo queryBydocument(Long id);

    /**
     * 作废功能
     * @param id
     */
    void updataForCrippled(Long id);

    /**
     * 入库操作
     * @param storeItemVo
     * @return
     */
    int bePutInStorage(StoreItemVo storeItemVo);

    /**
     * 出库的操作
     * @param storeItemVo
     * @return
     */
    int beOutInStorage(StoreItemVo storeItemVo);
}
