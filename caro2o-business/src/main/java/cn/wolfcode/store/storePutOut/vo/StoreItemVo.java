package cn.wolfcode.store.storePutOut.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class StoreItemVo {
    private String name;
    private Long store;   //仓库的id
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8" )
    private Date storeTime; //入库时间
    private String info;    //备注
    private List<StoreVo> storeDocument;



    private List<StorePutAndOutVo> storeItemVo;  //这是入库接收的仓库

}
