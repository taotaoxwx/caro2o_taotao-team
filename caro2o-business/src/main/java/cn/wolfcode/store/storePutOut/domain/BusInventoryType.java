package cn.wolfcode.store.storePutOut.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

/**
 * 出入库管理对象 bus_inventory_type
 * 
 * @author chen
 * @date 2023-06-16
 */
public class BusInventoryType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 类型 */
    @Excel(name = "类型")
    private Integer storeType;

    /** 仓库 */
    @Excel(name = "仓库")
    private String store;

    /** 物品数量 */
    @Excel(name = "物品数量")
    private Long goodsCount;

    /** 总金额 */
    @Excel(name = "总金额")
    private BigDecimal goodsAmount;

    /** 录入人 */
    @Excel(name = "录入人")
    private String inputName;

    /** 出入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8" )
    @Excel(name = "出入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date storeTime;

    /** 创建时间库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8" )
    @Excel(name = "出入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createTime;

    /** 状态 */
    @Excel(name = "状态")
    private Integer cancellationType;

    /**用来仓库的高级查询
     * */
    private String warehouseName;


    private String info;  //备注

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getId()
    {
        return id;
    }
    public void setStoreType(Integer storeType) 
    {
        this.storeType = storeType;
    }

    public Integer getStoreType() 
    {
        return storeType;
    }
    public void setStore(String store) 
    {
        this.store = store;
    }

    public String getStore() 
    {
        return store;
    }
    public void setGoodsCount(Long goodsCount) 
    {
        this.goodsCount = goodsCount;
    }

    public Long getGoodsCount() 
    {
        return goodsCount;
    }
    public void setGoodsAmount(BigDecimal goodsAmount) 
    {
        this.goodsAmount = goodsAmount;
    }

    public BigDecimal getGoodsAmount() 
    {
        return goodsAmount;
    }
    public void setInputName(String inputName) 
    {
        this.inputName = inputName;
    }

    public String getInputName() 
    {
        return inputName;
    }
    public void setStoreTime(Date storeTime) 
    {
        this.storeTime = storeTime;
    }

    public Date getStoreTime() 
    {
        return storeTime;
    }
    public void setCancellationType(Integer cancellationType) 
    {
        this.cancellationType = cancellationType;
    }

    public Integer getCancellationType() 
    {
        return cancellationType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("storeType", getStoreType())
            .append("store", getStore())
            .append("goodsCount", getGoodsCount())
            .append("goodsAmount", getGoodsAmount())
            .append("inputName", getInputName())
            .append("storeTime", getStoreTime())
            .append("createTime", getCreateTime())
            .append("cancellationType", getCancellationType())
            .toString();
    }
}
