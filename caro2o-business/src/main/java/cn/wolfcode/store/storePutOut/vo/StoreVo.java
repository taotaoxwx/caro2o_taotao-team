package cn.wolfcode.store.storePutOut.vo;

import cn.wolfcode.common.utils.sign.Base64;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class StoreVo {
    private Long id;   //id
    private String goods;   //物品
    private BigDecimal unitPrice; //价格
    private int quantity;   //数量
    private BigDecimal subtotal;  //小计

}
