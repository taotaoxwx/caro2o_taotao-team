package cn.wolfcode.store.storePutOut.mapper;

import java.util.List;
import cn.wolfcode.store.storePutOut.domain.BusInventoryType;
import cn.wolfcode.store.storePutOut.vo.EntryAndExitDetails;
import cn.wolfcode.store.storePutOut.vo.InventoryManagement;
import cn.wolfcode.store.storePutOut.vo.StoreVo;
import org.apache.ibatis.annotations.Param;

/**
 * 出入库管理Mapper接口
 * 
 * @author chen
 * @date 2023-06-16
 */
public interface BusInventoryTypeMapper 
{
    /**
     * 查询出入库管理
     * 
     * @param id 出入库管理主键
     * @return 出入库管理
     */
    public BusInventoryType selectBusInventoryTypeById(Long id);

    /**
     * 查询出入库管理列表
     * 
     * @param busInventoryType 出入库管理
     * @return 出入库管理集合
     */
    public List<BusInventoryType> selectBusInventoryTypeList(BusInventoryType busInventoryType);

    /**
     * 新增出入库管理
     * 
     * @param busInventoryType 出入库管理
     * @return 结果
     */
    public int insertBusInventoryType(BusInventoryType busInventoryType);

    /**
     * 修改出入库管理
     * 
     * @param busInventoryType 出入库管理
     * @return 结果
     */
    public int updateBusInventoryType(BusInventoryType busInventoryType);

    /**
     * 删除出入库管理
     * 
     * @param id 出入库管理主键
     * @return 结果
     */
    public int deleteBusInventoryTypeById(Long id);

    /**
     * 批量删除出入库管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusInventoryTypeByIds(Long[] ids);

    List<String> listForStore();

    Integer queryByStoreName(String store);

    List<StoreVo> queryByEntryAndExit(int storeid);

    void updateByCancellationType(@Param("id") Long id, @Param("cancellationType") int cancellationType);

    /**
     * 查询仓库中对应的单据明细
     * @param id
     * @return
     */
   List<EntryAndExitDetails> queryByEntryAndExitForid(Long id) ;


    /**
     * 根据id查询库存中的数据
     * @param id
     * @return
     */
    InventoryManagement queryByInventoryManagement(Long id);

    /**
     * 修改库存中的数量--作废
     * @param id
     * @param quantity
     */
    void updateByInventoryManagementAndQuantity(@Param("id") Long id, @Param("quantity") Long quantity);

    void AddupdateByInventoryManagementAndQuantity(@Param("id") Long id, @Param("quantity") Long quantity);

    void insertByEntryAndExitDetails(EntryAndExitDetails entryAndExitDetails);

    /**
     * 根据出入库中的id查询明细
     * @param id
     * @return
     */
    List<StoreVo> queryByEntryAndExitByid(Long id);
}
